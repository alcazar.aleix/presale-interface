import Web3 from "web3";
let selectedAccount;

export const  HandleConnectWallet = async () => {
    const provider = await window.ethereum
    provider
        .request({ method: 'eth_requestAccounts' })
        .then((accounts) => {
          selectedAccount = accounts[0];
          console.log(`Selected account is ${selectedAccount}`);
          //setWalletAccount(selectedAccount)
        })        
        .catch((err) => {
          console.log(err);
          return;
        });

      window.ethereum.on('accountsChanged', function (accounts) {
        selectedAccount = accounts[0];
        console.log(`Selected account changed to ${selectedAccount}`);
      });
      const web3 = new Web3(provider);
      

    
  }