import React, {Fragment, useState, useRef, useEffect} from "react";
import { v4 as uuidv4 } from "uuid";
//import bgImage from "./bgcard.png"
import bgImage from "./2.jpg"
// import bud from "./bud2.jpeg"
import budABI from "./bud.json"
import { HandleConnectWallet } from "./Web3Client.js"
import './App.css';
import { useWeb3React } from "@web3-react/core";
import { ethers } from "ethers";
import Web3 from "web3";
import presaleABI from "./presaleABI.json"
import { getDisplayName } from "next/dist/shared/lib/utils";
import {ProgressBar} from './ProgresBar.jsx'

let selectedAccount;
let isInitialized = false;
let bnb;
let amountPurchase;
let cnctedWallet = '';
let erc20Contract;
let contractAddr;
let erc20ContractBud;
let contractAddrBud;
let numTokens;
let numTokensReceived;
let resultInteract;
let errorInteract;


export function App() {

    const [tx, setTx] = useState([{
      txHash: '0'
    }]);
    const [txError, setTxError] = useState([{
      txHash: '0'
    }]);
    const [isFinalized, setIsFinalized] = useState(false);
    const [payed, setPayed] = useState(false);
    const [isClaimed, setIsClaimed] = useState(false);
    const [buyDisabled,setBuyDisabled] = useState(false);
    const [disable, setDisable] = useState(false);
    const [raisedCap, setRaisedCap] = useState(0);
    const [presaleIndex, setPresaleIndex] = useState(0);
    const mainBgImage = bgImage;

    const [introducedVal, setIntroducedVal] = useState("");
    const [val, setVal] = useState("");
    const [walletAccount, setWalletAccount] = useState("");
    const [isConnected,setIsConnected] = useState(false);
    const [tokenBalance, setTokenBalance] = useState("");
    const [state, setState] = useState(0);
    useEffect(() => {
      setIsConnected(walletAccount ?  true : false)
    }, [walletAccount])
    
    const updateProgress = (field, val) => {
      setState({ [field]: val });
    };
    useEffect(() => {
      canClaim();
      getBalance();
      getOwnBalance();
      handleConnectWallet();
      getPresaleIndex();
      getRiasedCap();
      updateProgress("percent", setState(raisedCap/1000));

    },[raisedCap,walletAccount,introducedVal,payed,presaleIndex,isClaimed,isFinalized])
    useEffect(() => {
      const interval = setInterval(() => {
        getRiasedCap();
        // canClaim();
        // getPresaleIndex();
      }, 1500);
      return () => clearInterval(interval);
    }, []);
    useEffect(() => {
      setVal(val ? val : 0)
    },[val])
      
    const handleSubmit = async (event) => {    
      event.preventDefault();
      amountPurchase = introducedVal;
      setIntroducedVal(introducedVal);
      await buy(amountPurchase);


    }
    const handleClaim = async (event) => {
      event.preventDefault();      
      await claim();
    }
    
    const  handleConnectWallet = async () => {
      const provider = await window.ethereum
      provider
          .request({ method: 'eth_requestAccounts' })
          .then((accounts) => {
            selectedAccount = accounts[0];
            console.log(`Selected account is ${selectedAccount}`);
            setWalletAccount(selectedAccount)
            cnctedWallet = selectedAccount
          })        
          .catch((err) => {
            console.log(err);
            return;
          });

        window.ethereum.on('accountsChanged', function (accounts) {
          selectedAccount = accounts[0];
          setWalletAccount(selectedAccount)
          cnctedWallet = selectedAccount
          console.log(`Selected account changed to ${selectedAccount}`);
        });
        const web3 = new Web3(provider);
        contractAddr= '0x0dfe392dC91C7F7534AFeC942393574e560ba3eF'//'0xEFDebe50372134CE1F5261B83825d82068a7A089'//'0xafb70fBe88f021eE4A8434f9b2A5CFAdFA5cd84A';//presale contract
        const erc20Abi = presaleABI
        erc20Contract =  new web3.eth.Contract(
          erc20Abi,
          contractAddr
        );

        //console.log(canClaim());
        isInitialized = true;

    }
    const getBalance = async () => {
      const balance = await window.ethereum.request({method: 'eth_getBalance', params: [walletAccount , 'latest']})
      const wei = parseInt(balance,16)
      bnb = (wei / Math.pow(10, 18))
      setVal(bnb) 
      console.log(bnb)
    }
    const getOwnBalance = async () => {
      const provider = await window.ethereum
      const web3 = new Web3(provider);
      if (!isInitialized) {
        await handleConnectWallet();
      }
      contractAddrBud= '0x04EE7158401439A7D390838b369330C08BE740A3';//bud contract
        const erc20AbiBud = budABI
        erc20ContractBud =  new web3.eth.Contract(
          erc20AbiBud,
          contractAddrBud
        );
        await erc20ContractBud.methods.balanceOf(walletAccount).call().then((output)=>{
          numTokensReceived = output;
          numTokensReceived = (numTokensReceived / Math.pow(10,18))
          console.log(numTokensReceived);
          setTokenBalance(numTokensReceived)
        });

  }
    
    const buy = async ({}) => {    
      const provider = await window.ethereum;
      const web3 = new Web3(provider);
      console.log(amountPurchase);
      const params= [
        {
          from: walletAccount,
          to: contractAddr,
          value: web3.utils.numberToHex(web3.utils.toWei(amountPurchase, 'ether' )) , // 2441406250
        },
      ];
      window.ethereum
        .request({
          method: 'eth_sendTransaction',
          params,
        })
        .then((result) => {
          console.log(result)
          resultInteract = result
          setTx(result)

        })
        .catch((error) => {
          console.log(error)
          errorInteract = error
          setTxError(error)

        });
        
        
      }

    const claim = async() =>{

      await erc20Contract.methods.claimTokens().send({from:walletAccount}).on('receipt', function(tokens){
        numTokens = tokens [0];
        
      })
      .catch((error) => {
        console.log(error)
        errorInteract = error
        setTxError(error)

      });
    
    }

    const canClaim = async () => {
      var currentTimeInSeconds=Math.floor(Date.now()/1000);
      var interval = 86400;
      await erc20Contract.methods.lastClaims(walletAccount,presaleIndex).call({from:walletAccount})
      .then((output) => {
        if(output){
          console.log(output + interval);
        parseInt(currentTimeInSeconds) >= parseInt(output) + interval  ? (
          setIsClaimed(false)
        ):(
          setIsClaimed(true)
        );
        }
      })
      await erc20Contract.methods.finalized().call()
      .then((outpu)=> {
        console.log(outpu)
        outpu ? (
          setIsFinalized(true)
        ):(
          setIsFinalized(false)
        );
      })
      console.log("Claim: " + isClaimed + " Finalized: " + isFinalized + " Ha pagado?: " + payed);
      if(!isClaimed && isFinalized && payed){
        setDisable(false)
      }else{
        setDisable(true)
      }
     }
    const getPresaleIndex = async () => {
      await erc20Contract.methods.presale_index().call().then((output)=>{
        setPresaleIndex(output);
        canBuy();
      });
    }
    const getRiasedCap = async () =>{
      await erc20Contract.methods.raisedCap().call().then((output) => {
        setRaisedCap(output / Math.pow(10, 18)); 
      });
    }
    const canBuy = async () => {
      await erc20Contract.methods.pays(walletAccount,presaleIndex).call().then((output)=>{
        setPayed(output);
        console.log(output);
        
      });
      await erc20Contract.methods.finalized().call()
      .then((outpu)=> {
        console.log(outpu)
        outpu ? (
          setIsFinalized(true)
        ):(
          setIsFinalized(false)
        );
      })
      if(!payed && !isFinalized){
        setBuyDisabled(false);
      }else{
        setBuyDisabled(true);
      }
    }
    const handleDisconnect = async () => {
      console.log("Disconnecting metamask")
      setIsConnected(false)
      setWalletAccount('')
    }
   
    return(
        <div className="App">

        <div className="main-card-wrapper" style={{ backgroundImage: `url(${mainBgImage})` }}>

        <div className="cards-wrapper" >

            <button onClick={handleConnectWallet}  className="main-connect-btn">{walletAccount ? walletAccount : "CONNECT WALLET"}</button>
            
            {isConnected ? (
              <div>
              <button onClick={getBalance}  className="main-connect-btn">{bnb ? bnb : "No BNB Balance"}</button>
              <button onClick={getOwnBalance}  className="main-connect-btn">{numTokensReceived ? numTokensReceived : "No Token Balance"}</button>

              </div>
            ):(
              <div></div>
            )}
            <div className="text-insert">INSERT YOUR BNB </div>           
              <div className="text-insert">
                        <form onSubmit={handleSubmit} >
                                <label>
                                    <input  
                                    type="number" 
                                    value={introducedVal}
                                    onChange={(e) => setIntroducedVal(e.target.value)}                                        
                                    />
                                    
                                </label>
                        </form>                  
                     <button disabled={buyDisabled} className="main-connect-btn" onClick={(e) =>handleSubmit(e) }>BUY</button>
                </div>
                  <button disabled={disable} className="main-connect-btn" onClick={(e) =>handleClaim(e)}>Claim</button>
                  <ProgressBar width={600} percent={raisedCap/1500} />

             </div>
            </div>
        </div>
   
    )
};